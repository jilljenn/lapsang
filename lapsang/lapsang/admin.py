# coding=utf8
from lapsang.models import Language, Sentence, Version, Item, Session
from django.forms import Textarea
from django.db import models
from django.contrib import admin, messages

class LanguageAdmin(admin.ModelAdmin):
    pass

class SentenceAdmin(admin.ModelAdmin):
    pass

class VersionAdmin(admin.ModelAdmin):
    pass

class ItemAdmin(admin.ModelAdmin):
    pass

class SessionAdmin(admin.ModelAdmin):
    pass

admin.site.register(Language, LanguageAdmin)
admin.site.register(Sentence, SentenceAdmin)
admin.site.register(Version, VersionAdmin)
admin.site.register(Item, ItemAdmin)
admin.site.register(Session, SessionAdmin)
