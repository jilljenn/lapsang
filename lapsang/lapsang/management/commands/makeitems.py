# coding=utf8
from django.core.management.base import BaseCommand, CommandError
from django.template import Context, Template
from django.template.defaultfilters import slugify
from lapsang.models import Sentence, Version, Item

class Command(BaseCommand):
    args = ''
    help = 'Makes items from sentences'
    def handle(self, *args, **options):
        for sentence in Sentence.objects.all():
            french = Version.objects.get(sentence=sentence, language__id=1)
            english = Version.objects.get(sentence=sentence, language__id=2)
            nb_words = len(english.text.split())
            for position in range(nb_words):
                Item(version_from=french, version_to=english, position=position).save()
                Item(version_from=english, version_to=french, position=position).save()
