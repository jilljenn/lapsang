from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.shortcuts import render, redirect, get_object_or_404
from lapsang.models import Item, Session
import datetime
import random

class ItemDetailView(DetailView):
    model = Item
    def get_object(self):
        obj = super(ItemDetailView, self).get_object()
        words = obj.version_to.text.split()
        # answer = words[obj.position - 1]
        words[obj.position] = '_' * 8
        obj.statement = ' '.join(words)
        return obj

def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def save_answer(request, item_id):
    item = get_object_or_404(Item, id=item_id)
    Session(user=request.user, item=item, answer=request.POST['answer'], end=datetime.datetime.now()).save()
    return redirect('/item/%d' % random.randint(1, 83))
