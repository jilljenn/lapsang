from django.conf.urls import patterns, include, url
from lapsang.views import ItemDetailView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'lapsang.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'lapsang.views.index'),
    url(r'^about/$', 'lapsang.views.about'),
    url(r'^item/(?P<pk>\d+)$', ItemDetailView.as_view()),
    url(r'^item/(?P<item_id>\d+)/answer/$', 'lapsang.views.save_answer'),
    url(r'^admin/', include(admin.site.urls)),
)
