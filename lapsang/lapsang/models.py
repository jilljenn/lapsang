# coding=utf8
from django.db import models
from django.contrib.auth.models import User

class Language(models.Model):
    name = models.CharField(max_length=32)
    def __unicode__(self):
        return self.name    

class Sentence(models.Model):
    concept = models.CharField(max_length=128)
    def __unicode__(self):
        return self.concept

class Version(models.Model):
    sentence = models.ForeignKey(Sentence)
    language = models.ForeignKey(Language)
    text = models.CharField(max_length=128)
    def __unicode__(self):
        return self.text

class Item(models.Model):
    version_from = models.ForeignKey(Version, related_name='from')
    version_to = models.ForeignKey(Version, related_name='to')
    position = models.IntegerField()

class Session(models.Model):
    user = models.ForeignKey(User)
    item = models.ForeignKey(Item)
    answer = models.TextField()
    begin = models.DateTimeField(auto_now=True)
    end = models.DateTimeField()
